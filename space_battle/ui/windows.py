import pygame
from space_battle.defaults import MENU_FONT_SIZE, WINDOW_WIDTH, WINDOW_HEIGHT, DEFAULT_FONT_SIZE, DEFAULT_HP, default_action, ENEMY_PARALLEL
from space_battle.entities.world_object import WorldObject


class Window(object):
    def __init__(self, *args, **kwargs):
        self.game_objects = []

    def add(self, obj):
        if isinstance(obj, WorldObject):
            self.game_objects.append(obj)

    def remove(self, obj):
        self.game_objects.remove(obj)

    def prepare_window(self, window):
        pass

    def key_control(self):
        pass

    def run(self, window):
        self.prepare_window(window)
        for game_object in self.game_objects:
            game_object.logic()
        self.key_control()
        for game_object in self.game_objects:
            game_object.draw(window)
        pygame.display.update()


class MenuWindow(Window):
    bgcolor = (51, 51, 51)
    last_choice = pygame.time.get_ticks()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)

    def key_control(self):
        keys = pygame.key.get_pressed()
        if not len(self.game_objects):
            return
        if self.last_choice and keys[self.last_choice]:
            return

        self.last_choice = None
        menu = self.game_objects[0]

        if keys[pygame.K_RETURN]:
            menu.action()

        if keys[pygame.K_UP]:
            menu.selected -= 1
            self.last_choice = pygame.K_UP
        if keys[pygame.K_DOWN]:
            menu.selected += 1
            self.last_choice = pygame.K_DOWN
        if menu.selected > len(menu.menu_items) - 1:
            menu.selected = 0
        if menu.selected < 0:
            menu.selected = len(menu.menu_items) - 1

    def prepare_window(self, window):
        window.fill(self.bgcolor)


class GameWindow(Window):
    bgcolor = (51, 51, 51)
    player = 0
    enemy = 1
    score = 0
    game_over = default_action

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', DEFAULT_FONT_SIZE)

    def key_control(self):
        keys = pygame.key.get_pressed()
        player = self.game_objects[self.player]
        enemy = self.game_objects[self.enemy]

        if keys[pygame.K_LEFT] and player.x > player.velocity:
            player.x -= player.velocity
        if keys[pygame.K_RIGHT] and player.x < (WINDOW_WIDTH - player.velocity - player.width):
            player.x += player.velocity
        if keys[pygame.K_UP] and player.y > player.velocity+ENEMY_PARALLEL+enemy.height:
            player.y -= player.velocity
        if keys[pygame.K_DOWN] and player.y < (WINDOW_HEIGHT - player.velocity - player.height):
            player.y += player.velocity
        if keys[pygame.K_SPACE]:
            player.shoot()
        if keys[pygame.K_ESCAPE]:
            self.game_over(self.score)

    def prepare_window(self, window):
        window.fill(self.bgcolor)
        window.blit(
            self.common_font.render(
                f'Score is: {self.score}',
                True,
                pygame.Color("Green"),
            ), (0, 0,)
        )

    def run(self, window):
        super().run(window)
        player = self.game_objects[self.player]
        enemy = self.game_objects[self.enemy]

        if enemy.dead:
            enemy.respawn()
            self.score += 100
        if player.dead:
            self.game_over(self.score)


class TextWindow(Window):
    bgcolor = (51, 51, 51)
    action = default_action

    def __init__(self, text='', score=0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = text
        self.score = score
        self.common_font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)

    def key_control(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RETURN]:
            self.action()

    def prepare_window(self, window):
        window.fill(self.bgcolor)
        text = self.text
        width1, height1 = self.common_font.size(text)
        width2, height2 = self.common_font.size(text)
        common_height = height1 + height2
        window.blit(
            self.common_font.render(
                self.text,
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width1 / 2,
                WINDOW_HEIGHT / 2 - common_height/2,
            )
        )
        window.blit(
            self.common_font.render(
                f'Score is: {self.score}',
                True,
                pygame.Color("Green"),
            ), (
                WINDOW_WIDTH / 2 - width2 / 2,
                WINDOW_HEIGHT / 2 + height2 / 2,
            )
        )
