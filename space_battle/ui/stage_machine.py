from space_battle.entities.player import Player
from space_battle.entities.enemy import Enemy
from space_battle.ui.windows import MenuWindow, GameWindow, TextWindow
from space_battle.entities.menu import Menu
from space_battle.entities.menu_item import MenuItem
from space_battle.defaults import WINDOW_HEIGHT, WINDOW_WIDTH, PLAYER_SPEED, ENEMY_PARALLEL
import pygame


class StageMachine(object):

    def __init__(self, *args, **kwargs):
        self.active = self.menu()
        self.flag_game = True

    def run(self, window, clock):
        while self.flag_game:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.flag_game = False
            if not self.flag_game:
                break

            self.active.run(window)

    def menu(self):
        menu_window = MenuWindow()
        menu = Menu(x=WINDOW_WIDTH / 2, y=WINDOW_HEIGHT / 2)
        game_start = MenuItem('Start')
        game_start.action = self.play
        menu.add(game_start)
        exit_item = MenuItem('Exit')
        exit_item.action = self.exit
        menu.add(exit_item)
        menu_window.add(menu)
        return menu_window

    def game(self):
        game_window = GameWindow()
        background = pygame.image.load('resources//images/s1200.jpg')
        player_img = pygame.image.load('resources//images/jet1.png')
        enemy_img = pygame.image.load('resources//images/hotpng.com (4).png')
        player = Player(
            player_img,
            game_window,
            x=(WINDOW_WIDTH - player_img.get_width()) / 2,
            y=WINDOW_HEIGHT - player_img.get_height(),
            width=player_img.get_width(),
            height=player_img.get_height(),
            velocity=PLAYER_SPEED,
            shoot_time=100,
            color='Green'
        )
        enemy = Enemy(
            enemy_img,
            game_window,
            y=ENEMY_PARALLEL-enemy_img.get_height(),
            color='Green',
            width=enemy_img.get_width(),
            height=enemy_img.get_height(),
        )
        player.target = enemy
        enemy.target = player
        game_window.add(player)
        game_window.player = len(game_window.game_objects) - 1
        game_window.add(enemy)
        game_window.enemy = len(game_window.game_objects) - 1
        game_window.game_over = self.end_game
        return game_window

    def game_over(self, score):
        game_over_window = TextWindow(text='Game Over!', score=score)
        game_over_window.action = self.exit
        return game_over_window

    def exit(self):
        self.flag_game = False

    def play(self):
        self.active = self.game()

    def end_game(self, score):
        self.active = self.game_over(score)

