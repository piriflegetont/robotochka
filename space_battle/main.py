# encoding utf-8
import pygame
import os
import sys

sys.path.append(os.path.join(sys.path[0], '..'))

from defaults import WINDOW_HEIGHT, WINDOW_WIDTH
from ui.stage_machine import StageMachine

if __name__ == '__main__':
    pygame.init()
    window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Game')
    clock = pygame.time.Clock()

    main = StageMachine()
    main.run(window, clock)

    pygame.quit()

