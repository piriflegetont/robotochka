from space_battle.entities.world_object import WorldObject
import pygame
from space_battle.defaults import MENU_FONT_SIZE, default_action


class MenuItem(WorldObject):
    highlite_color = (153, 102, 255)
    action = default_action

    def __init__(self, text, *args, **kwargs):
        super(MenuItem, self).__init__(*args, **kwargs)
        self.text = text
        self.font = pygame.font.Font('resources/fonts/joystix monospace.ttf', MENU_FONT_SIZE)
        width, height = self.font.size(text)
        self.width = width
        self.height = height

    def draw(self, window):
        window.blit(self.font.render(self.text, True, self.color), (self.x, self.y))
