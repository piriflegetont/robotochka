"""   Настройки персонажа  """
import pygame
import random

from space_battle.entities.player import Player
from space_battle.defaults import WINDOW_WIDTH, SHOOT_TIME, BULLET_SPEED, DEFAULT_HP, ENEMY_PARALLEL
from space_battle.entities.bullet import Bullet


class Enemy(Player):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.turn = -1
        self.fire = False
        self.last_shoot = 0

    def draw(self, window):
        if not self.dead:
            window.blit(self.img, (self.x, self.y))
            pygame.draw.rect(
                window,
                self.color,
                (self.x, self.y, self.width*(self.hp/DEFAULT_HP), 5),
            )

    def logic(self):
        if not self.dead:
            if self.y >= ENEMY_PARALLEL:
                # MOVE
                self.x += self.turn * self.velocity
                if self.x <= self.velocity or (self.x+self.width) >= (WINDOW_WIDTH-self.velocity):
                    self.turn *= -1
                # SHOOT
                current_tick = pygame.time.get_ticks()
                if (current_tick - self.last_shoot) > SHOOT_TIME:
                    self.shoot()
                    self.last_shoot = current_tick
            else:
                self.y += self.velocity

    def weapon(self):
        return Bullet(
            holder=self.holder,
            velocity=BULLET_SPEED,
            target=self.target,
            x=round(self.x + (self.width / 2)),
            y=self.y+self.height,
            color=pygame.Color('red'),
        )

    def respawn(self):
        directions = [1, -1]
        self.x = random.randint(self.width, WINDOW_WIDTH-self.width*2)
        self.y = 0-self.height
        self.turn = directions[random.randint(0, 1)]
        self.hp = DEFAULT_HP
        self.dead = False
