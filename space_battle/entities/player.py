"""   Настройки персонажа  """
import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.entities.mortal import Mortal
from space_battle.entities.bullet import Bullet
from space_battle.defaults import BULLET_SPEED, SHOOT_TIME, DEFAULT_HP


class Player(WorldObject, Mortal):
	def __init__(self, image, holder, target=None, shoot_time=SHOOT_TIME, *args, **kwargs):
		super(Player, self).__init__(*args, **kwargs)
		self.holder = holder
		self.window = pygame.display.get_surface()
		self.img = image
		self.last_shoot = 0
		self.target = target
		self.shoot_time = shoot_time

	def draw(self, window):
		if not self.dead:
			window.blit(self.img, (self.x, self.y))
			pygame.draw.rect(
				window,
				self.color,
				(self.x, self.y+self.height, self.width*(self.hp/DEFAULT_HP), 5),
			)

	def shoot(self):
		current_tick = pygame.time.get_ticks()
		if (current_tick - self.last_shoot) > self.shoot_time:
			self.last_shoot = current_tick
			self.weapon()

	def weapon(self):
		return Bullet(
				holder=self.holder,
				velocity=BULLET_SPEED * -1,
				target=self.target,
				x=round(self.x + (self.width / 2)),
				y=self.y,
				color=pygame.Color('red'),
			)
