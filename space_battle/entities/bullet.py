import pygame
from space_battle.entities.world_object import WorldObject
from space_battle.defaults import WINDOW_HEIGHT, BULLET_RADIUS, BULLET_DAMAGE


class Bullet(WorldObject):
    def __init__(self, target, holder, radius=BULLET_RADIUS, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.radius = radius
        self.target = target
        self.damage = BULLET_DAMAGE
        self.holder = holder
        self.holder.add(self)

    def draw(self, window):
        pygame.draw.circle(
            window,
            self.color,
            (self.x, self.y),
            self.radius,
        )

    def logic(self):
        # MOVE
        self.y += self.velocity
        if self.y < 0 or self.y > WINDOW_HEIGHT:
            self.destroy()
        else:
            self.collision_check()

    def collision_check(self):
        if (
            self.target.x <= self.x <= (self.target.x + self.target.width) and
            self.target.y <= self.y <= (self.target.y + self.target.height)
        ):
            self.target.hit(self.damage)
            self.destroy()

    def destroy(self):
        self.holder.remove(self)
